const merchantInfo = "/merchant/info"
const merchantVerification = "/merchant/verification"
const merchantLogin = "https://api.paymate.io/v1/merchant/login"
const merchantLoginForgot = "/merchant/login/forgot"
const merchantPassword = "/merchant/password"
const merchantApi = "/merchant/api"
const merchantApiStatus = "/merchant/api/status"
const merchantIpn = "/merchant/ipn"
const merchantRegister = "/merchant/register"
const merchantOrders = "/merchant/orders"
const merchantWithdraw = "/merchant/withdraw"
const merchant2fa = "/merchant/2fa"
const merchantResetInfo = "/merchant/reset/info?v="
const merchantResetPassword = "/merchant/reset/password"

const instance = axios.create({
    baseURL: "https://api.paymate.io/v1/",
    timeout: 0,
    headers: { 
        "Authorization": "Bearer " + window.sessionStorage.getItem("token")
    }
})