function renewIcon(seed) {
  var icon = blockies.create({
    // All options are optional
    seed: seed, // seed used to generate icon data, default: random
    //color: '#dfe', // to manually specify the icon color, default: random
    //bgcolor: '#aaa', // choose a different background color, default: random
    size: 8, // width/height of the icon in blocks, default: 8
    scale: 8 // width/height of each block in pixels, default: 4
    // spotcolor: '#000' // each pixel has a 13% chance of being of a third color,
    // default: random. Set to -1 to disable it. These "spots" create structures
    // that look like eyes, mouths and noses.
  })

  $(".profile-picture").attr("src", icon.toDataURL())
  $(".profile-picture").removeClass("d-none")
}

function validURL(str) {
  var pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
    "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
    "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
    "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
    "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i"
  ) // fragment locator
  return !!pattern.test(str)
}

var truncate = function(fullStr, strLen, separator) {
  if (fullStr.length <= strLen) return fullStr

  separator = separator || "..."

  var sepLen = separator.length,
    charsToShow = strLen - sepLen,
    frontChars = Math.ceil(charsToShow / 2),
    backChars = Math.floor(charsToShow / 2)

  return (
    fullStr.substr(0, frontChars) +
    separator +
    fullStr.substr(fullStr.length - backChars)
  )
}

function initAccountPage() {
  renewIcon(window.sessionStorage.getItem("merchant-id"))

  $("#mobile-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#desktop-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#sidebar-user-fullname").text(window.sessionStorage.getItem("name"))

  $("#mobile-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#desktop-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#sidebar-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#merchant-id-input").val(window.sessionStorage.getItem("merchant-id"))

  $("#mobile-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )
  $("#desktop-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )

  $("#mobile-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )
  $("#desktop-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )

  var today = new Date(Date.now()).toLocaleDateString()
  $("#mobile-date").text(today)
  $("#desktop-date").text(today)

  $("#fullname-input").val(window.sessionStorage.getItem("name"))
  $("#email-input").val(window.sessionStorage.getItem("email"))
  $("#organisation-name-input").val(
    window.sessionStorage.getItem("organisation-name")
  )

  $("#mobile-user-email").text(window.sessionStorage.getItem("email"))
  $("#desktop-user-email").text(window.sessionStorage.getItem("email"))
  $("#sidebar-user-email").text(window.sessionStorage.getItem("email"))

  $("#sidebar-user-organisation").text(
    window.sessionStorage.getItem("organisation-name")
  )

  if (window.sessionStorage.getItem("is-verified") == "true") {
    $("#mobile-confirm-email-button").text("Verified")
    $("#sidebar-confirm-email-button").text("Verified")
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
    $("#mobile-confirm-email-button").toggleClass("btn-warning")
    $("#sidebar-confirm-email-button").toggleClass("btn-warning")
    $("#mobile-confirm-email-button").toggleClass("btn-success")
    $("#sidebar-confirm-email-button").toggleClass("btn-success")

    $("#mobile-confirm-email-button").prop("disabled", true)
    $("#sidebar-confirm-email-button").prop("disabled", true)
  } else {
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
  }
}

function initCommonPage() {
  renewIcon(window.sessionStorage.getItem("merchant-id"))
  $("#mobile-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#desktop-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#sidebar-user-fullname").text(window.sessionStorage.getItem("name"))

  $("#mobile-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#desktop-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#sidebar-merchant-id").text(window.sessionStorage.getItem("merchant-id"))

  $("#mobile-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )
  $("#desktop-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )

  $("#mobile-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )
  $("#desktop-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )

  var today = new Date(Date.now()).toLocaleDateString()
  $("#mobile-date").text(today)
  $("#desktop-date").text(today)

  $("#mobile-user-email").text(window.sessionStorage.getItem("email"))
  $("#desktop-user-email").text(window.sessionStorage.getItem("email"))
  $("#sidebar-user-email").text(window.sessionStorage.getItem("email"))

  $("#sidebar-user-organisation").text(
    window.sessionStorage.getItem("organisation-name")
  )

  if (window.sessionStorage.getItem("is-verified") == "true") {
    $("#mobile-confirm-email-button").text("Verified")
    $("#sidebar-confirm-email-button").text("Verified")
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
    $("#mobile-confirm-email-button").toggleClass("btn-warning")
    $("#sidebar-confirm-email-button").toggleClass("btn-warning")
    $("#mobile-confirm-email-button").toggleClass("btn-success")
    $("#sidebar-confirm-email-button").toggleClass("btn-success")

    $("#mobile-confirm-email-button").prop("disabled", true)
    $("#sidebar-confirm-email-button").prop("disabled", true)
  } else {
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
  }
}

function initDashboardPage() {
  renewIcon(window.sessionStorage.getItem("merchant-id"))

  $("#mobile-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#desktop-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#sidebar-user-fullname").text(window.sessionStorage.getItem("name"))

  $("#mobile-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#desktop-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#sidebar-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#merchant-id").text(window.sessionStorage.getItem("merchant-id"))

  $("#wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )
  $("#mobile-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )
  $("#desktop-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )

  $("#mobile-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )
  $("#desktop-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )
  $("#orders").text(
    Number(window.sessionStorage.getItem("confirm-orders")) +
      Number(window.sessionStorage.getItem("pending-orders")) +
      " orders"
  )

  $("#statistic-confirmed").text(
    window.sessionStorage.getItem("confirm-orders")
  )
  $("#statistic-pending").text(window.sessionStorage.getItem("pending-orders"))

  var today = new Date(Date.now()).toLocaleDateString()
  $("#mobile-date").text(today)
  $("#desktop-date").text(today)
  $("#date").text(today)

  $("#mobile-user-email").text(window.sessionStorage.getItem("email"))
  $("#desktop-user-email").text(window.sessionStorage.getItem("email"))
  $("#sidebar-user-email").text(window.sessionStorage.getItem("email"))

  $("#sidebar-user-organisation").text(
    window.sessionStorage.getItem("organisation-name")
  )

  if (window.sessionStorage.getItem("is-verified") == "true") {
    $("#mobile-confirm-email-button").text("Verified")
    $("#sidebar-confirm-email-button").text("Verified")
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
    $("#mobile-confirm-email-button").toggleClass("btn-warning")
    $("#sidebar-confirm-email-button").toggleClass("btn-warning")
    $("#mobile-confirm-email-button").toggleClass("btn-success")
    $("#sidebar-confirm-email-button").toggleClass("btn-success")

    $("#mobile-confirm-email-button").prop("disabled", true)
    $("#sidebar-confirm-email-button").prop("disabled", true)
  } else {
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
  }
}

function initOrdersPage() {
  renewIcon(window.sessionStorage.getItem("merchant-id"))
  $("#mobile-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#desktop-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#sidebar-user-fullname").text(window.sessionStorage.getItem("name"))

  $("#mobile-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#desktop-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#sidebar-merchant-id").text(window.sessionStorage.getItem("merchant-id"))

  $("#mobile-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )
  $("#desktop-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )

  $("#mobile-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )
  $("#desktop-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )

  var today = new Date(Date.now()).toLocaleDateString()
  $("#mobile-date").text(today)
  $("#desktop-date").text(today)

  $("#mobile-user-email").text(window.sessionStorage.getItem("email"))
  $("#desktop-user-email").text(window.sessionStorage.getItem("email"))
  $("#sidebar-user-email").text(window.sessionStorage.getItem("email"))

  $("#sidebar-user-organisation").text(
    window.sessionStorage.getItem("organisation-name")
  )

  if (window.sessionStorage.getItem("is-verified") == "true") {
    $("#mobile-confirm-email-button").text("Verified")
    $("#desktop-confirm-email-button").text("Verified")
    $("#sidebar-confirm-email-button").text("Verified")
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#desktop-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
    $("#mobile-confirm-email-button").toggleClass("btn-warning")
    $("#desktop-confirm-email-button").toggleClass("btn-warning")
    $("#sidebar-confirm-email-button").toggleClass("btn-warning")
    $("#mobile-confirm-email-button").toggleClass("btn-success")
    $("#desktop-confirm-email-button").toggleClass("btn-success")
    $("#sidebar-confirm-email-button").toggleClass("btn-success")

    $("#mobile-confirm-email-button").prop("disabled", true)
    $("#desktop-confirm-email-button").prop("disabled", true)
    $("#sidebar-confirm-email-button").prop("disabled", true)
  } else {
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
  }
}

function initWalletPage() {
  renewIcon(window.sessionStorage.getItem("merchant-id"))

  $("#mobile-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#desktop-user-fullname").text(window.sessionStorage.getItem("name"))
  $("#sidebar-user-fullname").text(window.sessionStorage.getItem("name"))

  $("#mobile-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#desktop-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#sidebar-merchant-id").text(window.sessionStorage.getItem("merchant-id"))
  $("#merchant-id-input").val(window.sessionStorage.getItem("merchant-id"))

  $("#fiat-withdrawal-amount-em").prop(
    "title",
    "Your minimum withdrawal amount is " +
      (Number(window.sessionStorage.getItem("wallet-balance")) - 1) +
      " USDT"
  )
  $("#fiat-withdrawal-amount-em").attr(
    "data-original-title",
    "Your minimum withdrawal amount is " +
      (Number(window.sessionStorage.getItem("wallet-balance")) - 1) +
      " USDT"
  )
  $("#usdt-withdrawal-amount-em").prop(
    "title",
    "Your minimum withdrawal amount is " +
      (Number(window.sessionStorage.getItem("wallet-balance")) - 1) +
      1 +
      " USDT"
  )
  $("#usdt-withdrawal-amount-em").attr(
    "data-original-title",
    "Your minimum withdrawal amount is " +
      (Number(window.sessionStorage.getItem("wallet-balance")) - 1) +
      1 +
      " USDT"
  )
  // $("#confirm-withdrawal-tx-fee-em").prop(
  //   "title",
  //   "Maker fee: " + window.sessionStorage.getItem("tx-fee") + "%"
  // )
  // $("#confirm-withdrawal-tx-fee-em").attr(
  //   "data-original-title",
  //   "Maker fee: " + window.sessionStorage.getItem("tx-fee") + "%"
  // )

  $("#confirm-withdrawal-fee-usdt").text(
    "USDT " + window.sessionStorage.getItem("withdrawal-fee")
  )
  $("#wallet-balance").text(Number(window.sessionStorage.getItem("wallet-balance")).toFixed(2))
  $("#usdt-withdrawal-available-balance").text(
    "USDT " + Number(window.sessionStorage.getItem("wallet-balance")).toFixed(2)
  )
  $("#mobile-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )
  $("#desktop-wallet-balance").text(
    window.sessionStorage.getItem("wallet-balance") + " USDT"
  )
  $("#fiat-withdrawal-available-balance").text(
    "USDT" + Number(window.sessionStorage.getItem("wallet-balance")).toFixed(2)
  )
  $("#usdt-withdrawal-available-balance").text(
    "USDT " + Number(window.sessionStorage.getItem("wallet-balance")).toFixed(2)
  )

  $("#mobile-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )
  $("#desktop-orders").text(
    window.sessionStorage.getItem("confirm-orders") +
      window.sessionStorage.getItem("pending-orders") +
      " orders"
  )

  var today = new Date(Date.now()).toLocaleDateString()
  $("#mobile-date").text(today)
  $("#desktop-date").text(today)

  $("#mobile-user-email").text(window.sessionStorage.getItem("email"))
  $("#desktop-user-email").text(window.sessionStorage.getItem("email"))
  $("#sidebar-user-email").text(window.sessionStorage.getItem("email"))

  $("#sidebar-user-organisation").text(
    window.sessionStorage.getItem("organisation-name")
  )

  if (window.sessionStorage.getItem("is-verified") == "true") {
    $("#mobile-confirm-email-button").text("Verified")
    $("#sidebar-confirm-email-button").text("Verified")
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
    $("#mobile-confirm-email-button").toggleClass("btn-warning")
    $("#sidebar-confirm-email-button").toggleClass("btn-warning")
    $("#mobile-confirm-email-button").toggleClass("btn-success")
    $("#sidebar-confirm-email-button").toggleClass("btn-success")

    $("#mobile-confirm-email-button").prop("disabled", true)
    $("#sidebar-confirm-email-button").prop("disabled", true)
  } else {
    $("#mobile-confirm-email-button").toggleClass("d-none")
    $("#sidebar-confirm-email-button").toggleClass("d-none")
  }
}

/* paymate api */
// Post email verification
function postResendEmailVerification() {
  instance
    .post(merchantVerification, {})
    .then(function(response) {
      var data = response.data
      if (data.success) {
        toastr.success(data.message)
      }
    })
    .catch(function(error) {
      var data = error.response.data
      toastr.error(data.message)
    })
}

// Post 2FA
function post2FA() {
  instance
    .post(merchant2fa, {})
    .then(function(response) {
      var data = response.data
      toastr.success(data.message)
    })
    .catch(function(error) {
      var data = error.response.data
      toastr.error(data.message)
    })
}

// Post set ipn url
function postIpnUrl(ipnUrl) {
  // let axiosConfig = {
  //   headers: {
  //     "Content-Type": "application/json; charset=utf-8"
  //   }
  // }

  instance
    .post(merchantIpn, {
      url: ipnUrl
    })
    .then(function(response) {
      var data = response.data
      if (data.success) {
        toastr.success(data.message)

        $("#password-button").html($("#password-button").data("original-text"))
        $("#password-button").prop("disabled", false)
      }
    })
    .catch(function(error) {
      var data = error.response.data
      toastr.error(data.message)

      $("#password-button").html($("#password-button").data("original-text"))
      $("#password-button").prop("disabled", false)
    })
}

function getAccountUser() {
  instance
    .get(merchantInfo)
    .then(function(response) {
      var data = response.data
      if (data.success) {
        renewIcon(data.merchantId)

        $("#mobile-user-fullname").text(data.name)
        $("#desktop-user-fullname").text(data.name)
        $("#sidebar-user-fullname").text(data.name)

        $("#mobile-merchant-id").text(data.merchantId)
        $("#desktop-merchant-id").text(data.merchantId)
        $("#sidebar-merchant-id").text(data.merchantId)

        $("#mobile-wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#desktop-wallet-balance").text(data.wallet.withdrawableBal + " USDT")

        $("#mobile-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )
        $("#desktop-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )

        var today = new Date(Date.now()).toLocaleDateString()
        $("#mobile-date").text(today)
        $("#desktop-date").text(today)

        $("#fullname-input").val(data.name)
        $("#email-input").val(data.email)
        $("#organisation-name-input").val(data.orgName)

        $("#mobile-user-email").text(data.email)
        $("#desktop-user-email").text(data.email)
        $("#sidebar-user-email").text(data.email)

        $("#sidebar-user-organisation").text(data.orgName)

        if (!data.isVerified) {
          $("#email-alert").removeClass("d-none")
        }
      }
    })
    .catch(function(error) {
      window.location.replace("./login.html")
    })
}

function getCommonUser() {
  instance
    .get(merchantInfo)
    .then(function(response) {
      var data = response.data
      if (data.success) {
        renewIcon(data.merchantId)

        $("#mobile-user-fullname").text(data.name)
        $("#desktop-user-fullname").text(data.name)
        $("#sidebar-user-fullname").text(data.name)

        $("#mobile-merchant-id").text(data.merchantId)
        $("#desktop-merchant-id").text(data.merchantId)
        $("#sidebar-merchant-id").text(data.merchantId)

        $("#mobile-wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#desktop-wallet-balance").text(data.wallet.withdrawableBal + " USDT")

        $("#mobile-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )
        $("#desktop-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )

        var today = new Date(Date.now()).toLocaleDateString()
        $("#mobile-date").text(today)
        $("#desktop-date").text(today)

        $("#mobile-user-email").text(data.email)
        $("#desktop-user-email").text(data.email)
        $("#sidebar-user-email").text(data.email)

        $("#sidebar-user-organisation").text(data.orgName)
      }
    })
    .catch(function(error) {
      window.location.replace("./login.html")
    })
}

function getDashboardUser() {
  instance
    .get(merchantInfo)
    .then(function(response) {
      var data = response.data
      if (data.success) {
        renewIcon(data.merchantId)

        $("#mobile-user-fullname").text(data.name)
        $("#desktop-user-fullname").text(data.name)
        $("#sidebar-user-fullname").text(data.name)

        $("#mobile-merchant-id").text(data.merchantId)
        $("#desktop-merchant-id").text(data.merchantId)
        $("#sidebar-merchant-id").text(data.merchantId)
        $("#merchant-id").text(data.merchantId)

        $("#wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#mobile-wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#desktop-wallet-balance").text(data.wallet.withdrawableBal + " USDT")

        $("#desktop-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )
        $("#mobile-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )
        $("#orders").text(
          Number(data.orders.confirmCount) +
            Number(data.orders.pendingCount) +
            " orders"
        )

        $("#statistic-confirmed").text(data.orders.confirmCount)
        $("#statistic-pending").text(data.orders.pendingCount)

        var today = new Date(Date.now()).toLocaleDateString()
        $("#mobile-date").text(today)
        $("#desktop-date").text(today)
        $("#date").text(today)

        $("#mobile-user-email").text(data.email)
        $("#desktop-user-email").text(data.email)
        $("#sidebar-user-email").text(data.email)

        $("#sidebar-user-organisation").text(data.orgName)
      }
    })
    .catch(function(error) {
      window.location.replace("./login.html")
    })
}

function getOrdersUser() {
  instance
    .get(merchantInfo)
    .then(function(response) {
      var data = response.data
      if (data.success) {
        renewIcon(data.merchantId)

        $("#mobile-user-fullname").text(data.name)
        $("#desktop-user-fullname").text(data.name)
        $("#sidebar-user-fullname").text(data.name)

        $("#mobile-merchant-id").text(data.merchantId)
        $("#desktop-merchant-id").text(data.merchantId)
        $("#sidebar-merchant-id").text(data.merchantId)
        $("#merchant-id").text(data.merchantId)

        $("#wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#mobile-wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#desktop-wallet-balance").text(data.wallet.withdrawableBal + " USDT")

        $("#desktop-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )
        $("#mobile-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )
        
        $("#orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )

        $("#statistic-confirmed").text(data.orders.confirmCount)
        $("#statistic-pending").text(data.orders.pendingCount)

        var today = new Date(Date.now()).toLocaleDateString()
        $("#mobile-date").text(today)
        $("#desktop-date").text(today)
        $("#date").text(today)

        $("#mobile-user-email").text(data.email)
        $("#desktop-user-email").text(data.email)
        $("#sidebar-user-email").text(data.email)

        $("#sidebar-user-organisation").text(data.orgName)
      }
    })
    .catch(function(error) {
      window.sessionStorage.setItem("token", "")
      window.location.replace("./login.html")
    })
}

function getWalletUser() {
  instance
    .get(merchantInfo)
    .then(function(response) {
      var data = response.data
      if (data.success) {
        renewIcon(data.merchantId)

        $("#mobile-user-fullname").text(data.name)
        $("#desktop-user-fullname").text(data.name)
        $("#sidebar-user-fullname").text(data.name)

        $("#mobile-merchant-id").text(data.merchantId)
        $("#desktop-merchant-id").text(data.merchantId)
        $("#sidebar-merchant-id").text(data.merchantId)
        $("#merchant-id-input").val(data.merchantId)

        $("#fiat-withdrawal-amount-em").prop(
          "title",
          "Your minimum withdrawal amount is " +
            (Number(data.wallet.withdrawableBal) - 1) +
            " USDT"
        )
        $("#fiat-withdrawal-amount-em").attr(
          "data-original-title",
          "Your minimum withdrawal amount is " +
            (Number(data.wallet.withdrawableBal) - 1) +
            " USDT"
        )
        $("#usdt-withdrawal-amount-em").prop(
          "title",
          "Your minimum withdrawal amount is " +
            (Number(data.wallet.withdrawableBal) - 1) +
            " USDT"
        )
        $("#usdt-withdrawal-amount-em").attr(
          "data-original-title",
          "Your minimum withdrawal amount is " +
            (Number(data.wallet.withdrawableBal) - 1) +
            " USDT"
        )
        // $("#confirm-withdrawal-tx-fee-em").prop(
        //   "title",
        //   "Maker fee: " + data.fees.txFee + "%"
        // )
        // $("#confirm-withdrawal-tx-fee-em").attr(
        //   "data-original-title",
        //   "Maker fee: " + data.fees.txFee + "%"
        // )
        $("#confirm-withdrawal-fee-usdt").text(
          "USDT " + data.fees.withdrawalFee
        )
        $("#wallet-balance").text(data.wallet.withdrawableBal)
        $("#mobile-wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#desktop-wallet-balance").text(data.wallet.withdrawableBal + " USDT")
        $("#fiat-withdrawal-available-balance").text(
          "USDT " + data.wallet.withdrawableBal
        )
        $("#usdt-withdrawal-available-balance").text(
          "USDT " + data.wallet.withdrawableBal
        )

        $("#desktop-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )
        $("#mobile-orders").text(
          data.orders.confirmCount + data.orders.pendingCount + " orders"
        )

        var today = new Date(Date.now()).toLocaleDateString()
        $("#mobile-date").text(today)
        $("#desktop-date").text(today)

        $("#mobile-user-email").text(data.email)
        $("#desktop-user-email").text(data.email)
        $("#sidebar-user-email").text(data.email)

        $("#sending-fee").text(data.wallet.totalSendingFee)
        $("#deduction").text(data.wallet.totalFees)

        $("#sidebar-user-organisation").text(data.orgName)
      }
    })
    .catch(function(error) {
      window.location.replace("./login.html")
    })
}
