Paymate
=========================================

Getting started
---------------

To get started, install the required node modules:

```
npm install
```

Then issue the following command to run the server:

```
npm start
```

Navigate to http://localhost:8080/ and files from the public folder will start being served.